package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIdRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private Status status;

    public TaskChangeStatusByIdRequest(
            @Nullable String token
    ) {
        super(token);
    }

    public TaskChangeStatusByIdRequest(
            @Nullable String token,
            @Nullable String taskId,
            @Nullable Status status
    ) {
        super(token);
        this.taskId = taskId;
        this.status = status;
    }

}
