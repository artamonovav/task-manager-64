package ru.t1.artamonov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.artamonov.tm.model.User;


@Repository
public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(String login);

    @Nullable
    User findByEmail(String email);

}
