package ru.t1.artamonov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.artamonov.tm")
public class ApplicationConfiguration {
}
